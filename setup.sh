#!/usr/bin/env bash

git submodule init
git submodule update --recursive

python3 setup.py install --user pylsl pyserial
pip install --user git+https://gitlab.com/wrenlab/metalearn.git
