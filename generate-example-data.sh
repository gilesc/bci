#!/usr/bin/env bash

sed 1d OpenBCI_GUI/OpenBCI_GUI/SavedData/Sample_Data/openBCI_2013-12-24_meditation.txt \
    | sed 's/,\ /\t/g' \
    | awk 'BEGIN {OFS=FS="\t"} { print NR/250,$2,$3,$4,$5,$6,$7,$8 }' \
    | gzip \
    > data/meditation.X.tsv.gz

echo -ne "0\t0\n300\t1\n1200\t0\n" > data/meditation.y.tsv
