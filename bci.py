import os
import datetime
import gzip
import math
import sys
from abc import ABC, abstractmethod
from collections import namedtuple

import numpy as np
import pandas as pd
import scipy.interpolate
import pylsl

import metalearn

class Chunk(object):
    DEFAULT_FREQUENCIES = np.arange(10, 61, step=0.5)

    def __init__(self, ts, X, y, sample_rate):
        self.ts = ts
        self.X = X
        self.y = y
        self.sample_rate = sample_rate

    @property
    def n_channels(self):
        return self.X.shape[1]

    def fft(self, frequencies=None):
        if frequencies is None:
            frequencies = self.DEFAULT_FREQUENCIES

        o = []
        for channel in range(self.n_channels):
            data = self.X.iloc[:,channel]
            fft = np.fft.fft(data)
            bins = np.fft.fftfreq(fft.shape[0], d=(1 / self.sample_rate))
            # 10 to 60 Hz
            fn = scipy.interpolate.interp1d(bins, fft)
            y = fn(frequencies)
            y = (np.real(y) ** 2 + np.imag(y) ** 2) ** 0.5
            o.append(y)
        return np.vstack(o).T

class Source(ABC):
    @abstractmethod
    def stream(self, size=3, step=1):
        pass

class StaticSource(Source):
    def __init__(self, X_path, y_path, sample_rate=250):
        self.sample_rate = sample_rate
        """
        self.X = pd.read_csv(path, skiprows=1, header=None).drop(0, axis=1)
        self.ts = np.arange(self.X.shape[0]) / sample_rate
        y = np.concatenate([
            np.zeros(sample_rate * 5 * 60),
            np.ones(sample_rate * 15 * 60)
        ])
        self.y = np.concatenate([y, np.zeros(self.X.shape[0] - len(y))])
        """
        X = pd.read_csv(X_path, header=None, compression="gzip", sep="\t")
        self.X = X.iloc[:,1:]
        self.ts = X.iloc[:,0]

        y_df = pd.read_csv(y_path, header=None, sep="\t").sort_values(0)
        y_ts = y_df.iloc[:,0]
        y_state = y_df.iloc[:,1]
        ix = np.searchsorted(y_ts, self.ts)
        self.y = y_state.iloc[ix - 1]

    def stream(self, size=3, step=1):
        assert size % step == 0
        seconds = self.X.shape[0] / self.sample_rate
        for i in range(0, size, step):
            windows = np.arange(i, math.floor(seconds), size)
            for j in range(len(windows) - 1):
                start = windows[j]*self.sample_rate
                end = windows[j+1]*self.sample_rate

                # Only use chunks that don't fall on a boundary between categories
                y = list(set(self.y[start:end]))
                if len(y) != 1:
                    continue
                y = int(y[0])
                yield Chunk(self.ts[start:end], self.X.iloc[start:end,:], y, self.sample_rate)

    def problem(self):
        X,y = [],[]
        for chunk in self.stream():
            X.append(np.array(chunk.fft().flat))
            y.append(chunk.y)
        X = pd.DataFrame(np.vstack(X))
        y = pd.Series(np.array(y))
        return metalearn.problem.BinaryProblem(X,y)


class LiveSource(Source):
    def __init__(self):
        streams = pylsl.pylsl.resolve_stream("type", "EEG")
        # FIXME: get sampling rate from pylsl StreamInfo
        self.sample_rate = 250
        self.inlet = pylsl.StreamInlet(streams[0])

    def stream_to(self, path):
        with gzip.open(path, "wt") as h:
            try:
                for i,chunk in enumerate(self.stream()):
                    print("Chunk", i, "written", file=sys.stderr)
                    o = np.zeros((chunk.ts.shape[0],1))
                    o[:,0] = chunk.ts
                    o = np.concatenate([o, np.array(chunk.X)], axis=1)
                    pd.DataFrame(o).to_csv(h, mode="a", header=False, index=False, sep="\t")
            finally:
                h.close()

    def stream(self, size=3):
        ts,X = [],[]
        while True:
            # FIXME: figure out how to map b/t LSL timestamp and unix timestamp
            sample, ts_ = self.inlet.pull_sample()
            ts_ = datetime.datetime.now().timestamp()
            ts.append(ts_)
            X.append(tuple(sample))
            if len(ts) == self.sample_rate * size:
                ts = pd.Series(ts)
                X = pd.DataFrame.from_records(X)
                yield Chunk(ts, X, None, self.sample_rate)
                ts,X = [],[]

def test_classification():
    import sklearn.linear_model

    sample_rate = 250
    path = "OpenBCI_GUI/OpenBCI_GUI/SavedData/Sample_Data/openBCI_2013-12-24_meditation.txt"
    source = StaticSource("data/meditation.X.tsv.gz", "data/meditation.y.tsv")
    p = source.problem()
    model = sklearn.linear_model.LogisticRegression()
    rs = p.cross_validate(model)
    print(rs.metrics())
    
    """
    Here is a complete example of how to train a model using saved data as
    above, and then classify on the fly with live data (probably could make a
    slicker API for this, and I cannot test this now so it may have minor bugs
    but the concept is sound):


    train = StaticSource("data/meditation.X.tsv.gz", "data/meditation.y.tsv")
    p = train.problem()
    model = sklearn.linear_model.LogisticRegression()
    model.fit(p.X, p.y)

    live = LiveSource()
    for chunk in live.stream():
        fft = pd.Series(chunk.fft()).to_frame()
        p = np.array(model.predict_log_proba(fft))[0,:]
        p = pd.Series(p, index=model.classes_)
        # p will be the log probabilities for each class
        do_something(p)
    """
