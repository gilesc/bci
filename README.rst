========================================================
BCI - real-time classification for EEG data from OpenBCI
========================================================

Requirements
============

- Python 3
- numpy
- scipy
- A OpenBCI Cyton board with Bluetooth dongle

Usage
=====

After cloning the repository, run:

.. code-block:: bash
    $ ./setup.sh

This will retrieve the git submodules and install necessary Python packages.
    
Streaming data
--------------

1. Plug in the USB bluetooth dongle
2. Find its port (should be something like "/dev/ttyUSB0" on Linux)
3. Add yourself to the group that owns that file (only once per computer):

.. code-block:: bash
    $ ls -la /dev/ttyUSB0
    
     (find the group, on Arch Linux it is "uucp")

    $ sudo usermod -a -G uucp $USER

4. Open a terminal and start the data stream:

.. code-block:: bash

    $ ./stream.sh

5. A window will pop up if the stream started correctly. Type ``/start<enter>``
   to begin the stream and ``/exit<enter>`` to stop it.

Saving data
-----------

Once you have started ``stream.sh``, open another terminal in the bci directory and start a Python shell. Then, run:

.. code-block:: python

    >>> import bci
    >>> s = bci.LiveSource()
    >>> s.stream_to("/path/to/data.tsv.gz")

This will save data as it is acquired to the provided path in gzipped TSV format.

Training & classification (static)
----------------------------------

Output classification is performed on "chunks" -- that is, time slices of EEG
data. By default, these chunks are 3 seconds long. The magnitude of each
frequency between 10 and 60 Hz is determined by FFT for each chunk, and the
vector of frequency magnitudes is used as the training data for a ML model.

The other piece of information needed is the brain states for each chunk. Thus,
to train a model statically, you will need to record UNIX timestamps for each
state to go with the recorded data (as obtained using the method described in
the previous section "Saving data").

To see an example of the data formats needed, first run
``generate-example-data.sh``. This will output two files into the ``data/``
subdirectory:

- ``meditation.X.tsv.gz``
- ``meditation.y.tsv``

The ``meditation.X.tsv.gz`` file is a file in the same format as would be
output by the ``LiveSource.stream_to`` function (although with synthetic
timestamps, because the original source data does not include them). The
``meditation.y.tsv`` file provides the mapping from timestamps to states. That
file has two tab-delimited columns: the first column is the timestamp
indicating the start of a new state and the second column is the state label
(in this case, "0" is the control state and "1" is meditation).

In the function ``bci.test_classification``, there is an example of how to get
cross-validation metrics for a model as well as an example of how to first
train a model, and then run a callback function on the predicted probability
distribution over brain states using live data (NB: I would highly recommend
testing this with junk data and states to test the entire pipeline before
attempting it live).

Training & classification (online)
----------------------------------

This section will describe how to train a model that updates as the EEG is
running (TODO).
